//Name = Johan Vermaak
//Date = 21/03/2018
//ID = 10015918

BMI = 0;
weight = 0;
height = 0;

weight = Number(prompt("Please input your weight in KG"));
height = Number(prompt("Please input your height in M"));

BMI = weight / (height*height);

if (BMI < 18.5){
    console.log("Your BMI Category is 'Underweight'");
}else if (BMI >=18.5 && BMI < 25){
    console.log("Your BMI Category is 'Normal Weight'");
}else if (BMI >= 25 && BMI_< 30){
    console.log("Your BMI Category is 'Overweight'");
}else if (BMI >= 30){
    console.log("Your BMI Category is 'Obese'");
}
console.log(`Your weight input was: ${weight} kg\nYour height input was: ${height} M\nBody Mass Index: ${BMI.toFixed(1)}`)