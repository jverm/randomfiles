month = 1;
rainfall = 0;
total = 0;

for(month = 1; month <= 12; month++){
    rainfall = Number(prompt(`Enter the rainfall (mm) for month ${month}:`));
    console.log(`Month: ${month} with a rainfall of ${rainfall}mm`);
    total = total + rainfall;
}
console.log(`\nTotal amount of yearly rain in mm is: ${total}mm`);